const express = require('express')
const pmongo = require('then-mongo')
const cors = require('cors')
const bodyParser = require('body-parser')

const DB_USER = 'root'
const DB_PASSWORD = 'root'

let app = express();
app.use(cors())
app.use(bodyParser.json())

app.get('/',(req,res)=>res.send('sup'))

app.route('/users/')
.get(async(req,res)=>res.send(await users.find({}).toArray()))
.post(async(req,res)=>res.send(await users.insert(req.body)))
.delete(async(req,res)=>{res.send(await users.remove({"_id":pmongo.ObjectID(req.body.id)}))})

let db = pmongo(`mongodb://${DB_USER}:${DB_PASSWORD}@ds129386.mlab.com:29386/kompare`)
let users = db.collection('users');
app.listen(8080,()=>console.log(`Mongo API listening on port 8080`))

